/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author faisalijaz1
 */
@ManagedBean(name="getvalue")
@ViewScoped
public class GetValue implements Serializable{
  private String content;  
  private String inputVal;
    /**
     * Creates a new instance of GetValue
     */
    public GetValue() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getInputVal() {
        return inputVal;
    }

    public void setInputVal(String inputVal) {
        this.inputVal = inputVal;
    }
    
    public void saveListener1(){
         content = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("receivevalue");
      FacesContext.getCurrentInstance().addMessage("Value From TextBox is ", new FacesMessage(content));
      System.out.println("enterrrrrrrrrrr   "+content); 
        
        
        
    }
}
