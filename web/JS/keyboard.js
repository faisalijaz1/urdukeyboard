var lang = 1;

var urdukey_phonetic = [ 32, 33, 45, 35, 1610, 1642, 1619, 8228, 41, 40, 1612, 1619, 1548, 1652, 1748, 47, 1776, 1777, 1778, 1779, 1780, 1781, 1782, 1783, 1784, 1785, 58, 1563, 1616, 1572, 1614, 1567, 1621, 1570, 57344, 1579, 1672, 57345, 57347, 1594, 1581, 1648, 1590, 1582, 57346, 1621, 1722, 1577, 1615, 1618, 1681, 1589, 1657, 1569, 1592, 65018, 1688, 1610, 1584, 1607, 65010, 57348, 1618, 1617, 1611, 1575, 1576, 1670, 1583, 1593, 1601, 1711, 1726, 1740, 1580, 1705, 1604, 1605, 1606, 1729, 1662, 1602, 1585, 1587, 1578, 1574, 1591, 1608, 1588, 1746, 1586, 8216, 1652, 8217, 1613 ];

var urdukey_urdu98 = [ 32, 33, 34, 35, 1764, 1642, 1619, 39, 41, 40, 42, 43, 1548, 45, 1748, 47, 1776, 1777, 1778, 1779, 1780, 1781, 1782, 1783, 1784, 1785, 58, 1563, 64830, 61, 64831, 1567, 1621, 1616, 1672, 57345, 1584, 1594, 1611, 1613, 1570, 1569, 1615, 1612, 57348, 65018, 1681, 1607, 57344, 1572, 1586, 1617, 1657, 1582, 1574, 1614, 1590, 1610, 1688, 1726, 1648, 1591, 1618, 57347, 1577, 1587, 1576, 1579, 1583, 1593, 1601, 1711, 1575, 1746, 1580, 1705, 1604, 1605, 1606, 1729, 1662, 1602, 1585, 1588, 1578, 1581, 1608, 1670, 1589, 1740, 1722, 57346, 1652, 1592, 65010 ];

function FKeyDown() {
    if (window.event.shiftKey && window.event.altKey) {
        if (0 == lang) {
            lang = 1;
            window.defaultStatus = "Urdu Mode";
        } else {
            lang = 0;
            window.defaultStatus = "English Mode";
        }
        return false;
    }
    return true;
}

function FKeyPress() {
    var a = window.event.keyCode;
    if (a < 32 || a >= 255) return;
    if (1 == lang) if (32 == a && window.event.shiftKey) window.event.keyCode = 8204; else window.event.keyCode = urdukey_urdu98[a - 32];
    return true;
}

function LangFar(a) {
    a.style.textAlign = "right";
    a.style.direction = "rtl";
    a.focus();
    lang = 1;
}

function LangEng(a) {
    a.style.textAlign = "left";
    a.style.direction = "ltr";
    a.focus();
    lang = 0;
}

function hurf(a) {
    if ("alifmada" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "آ";
    if ("alif" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ا";
    if ("bay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ب";
    if ("pay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "پ";
    if ("tay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ت";
    if ("thay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ٹ";
    if ("say" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ث";
    if ("jim" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ج";
    if ("hay1" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ح";
    if ("khay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "خ";
    if ("chay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "چ";
    if ("dal" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "د";
    if ("dhal" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ڈ";
    if ("zal" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ذ";
    if ("ray" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ر";
    if ("zay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ز";
    if ("kaf" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ک";
    if ("gaf" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "گ";
    if ("qaf" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ق";
    if ("fay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ف";
    if ("gain" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "غ";
    if ("ain" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ع";
    if ("zhoy" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ظ";
    if ("thoy" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ط";
    if ("duad" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ض";
    if ("suad" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ص";
    if ("sheen" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ش";
    if ("seen" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "س";
    if ("zhay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ژ";
    if ("rahy" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ڑ";
    if ("pbuh" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ﷺ";
    if ("Allah" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "لله";
    if ("bariya" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ے";
    if ("yamada" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ئ";
    if ("ya" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ی";
    if ("dochachmihay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ھ";
    if ("hay" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ہ";
    if ("hamza" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ء";
    if ("wowmada" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ؤ";
    if ("wow" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "و";
    if ("gunah" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ں";
    if ("noon" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ن";
    if ("mim" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "م";
    if ("lam" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "ل";
    if ("space" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + " ";
    if ("entr" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\r";
    if ("bspace" == a) {
        varbk = document.getElementById("txt1").value;
        varbk = varbk.substring(0, varbk.length - 1);
        document.getElementById("txt1").value = varbk;
    }
    if ("dash" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "۔";
    if ("salam" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rاسلام علیکم";
    if ("دل کی گہرائیوں سے دلی عید مبارک" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rدل کی گہرائیوں سے دلی عید مبارک۔";
    if ("ہم سب کی طرف سے دلی عید مبارک" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rہم سب کی طرف سے دلی عید مبارک۔";
    if ("آپ کو نیک دعاؤں اور تمناؤں کے ساتھ عید مبارک" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rآپ کو نیک دعاؤں اور تمناؤں کے ساتھ عید مبارک۔";
    if ("آپ کو خوشیوں سے بھرپور عید مبارک ہو" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rآپ کو خوشیوں سے بھرپور عید مبارک ہو۔";
    if ("رحمتوں اور برکتوں والا مہینہ مبارک ہو" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rرحمتوں اور برکتوں والا مہینہ مبارک ہو۔";
    if ("مغفرتوں والا مہینہ مبارک ہو" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rمغفرتوں والا مہینہ مبارک ہو۔";
    if ("اﷲ آپ کو اس مہینے میں ہزاروں خوشیاں عطا فرمائے" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rاﷲ آپ کو اس مہینے میں ہزاروں خوشیاں عطا فرمائے۔";
    if ("اﷲ آپ کو اس مہینے میں ہزاروں برکتیں و نعمتیں عطا فرمائے" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rاﷲ آپ کو اس مہینے میں ہزاروں برکتیں و نعمتیں عطا فرمائے۔";
    if ("سالگرہ مبارک" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rسالگرہ مبارک";
    if ("آپ کو سالگرہ کا دن مبارک ہو" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rآپ کو سالگرہ کا دن مبارک ہو";
    if ("میری دعا ہے کہ آپ کو ہزاروں سالگرہ نصیب ہوں" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rمیری دعا ہے کہ آپ کو ہزاروں سالگرہ نصیب ہوں";
    if ("مجھے امید ہے آپ کی زندگی ہمیشہ خوشیوں سے بھری ہوگی" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rمجھے امید ہے آپ کی زندگی ہمیشہ خوشیوں سے بھری ہوگی";
    if ("کاش آپ ابھی میری زندگی میں آجاؤ" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rکاش آپ ابھی میری زندگی میں آجاؤ";
    if ("آپ کی یاد دل کو تڑپا رہی ہے" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rآپ کی یاد دل کو تڑپا رہی ہے";
    if ("آدھے آدھے تھے ہم دونوں مل جاتے تو پورے ہوتے" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rآدھے آدھے تھے ہم دونوں\rمل جاتے تو پورے ہوتے";
    if ("آپ کی نظرِ کرم کا شکریہ" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rآپ کی نظرِ کرم کا شکریہ";
    if ("ہمیں یاد کرنے پر دل و جان سے آپ کا شکریہ" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rہمیں یاد کرنے پر دل و جان سے آپ کا شکریہ";
    if ("میں بہت خوش قسمت ہوں کہ مجھے آپ جیسا بہترین دوست ملا" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rمیں بہت خوش قسمت ہوں کہ مجھے آپ جیسا بہترین دوست ملا";
    if ("میں اﷲ سے دعا کرتا ہوں کہ میری اور آپ کی دوستی ہمیشہ سلامت رہے" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rمیں اﷲ سے دعا کرتا ہوں کہ میری اور آپ کی دوستی ہمیشہ سلامت رہے";
    if ("خدا کرے میری اور آپ کی دوستی کو کسی کی نظر نہ لگے" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rخدا کرے میری اور آپ کی دوستی کو کسی کی نظر نہ لگے";
    if ("آپ کو یہ کامیابی مبارک ہو" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rآپ کو یہ کامیابی مبارک ہو";
    if ("اﷲ آپ کو ایسی ہزار کامیابیاں عطا فرمائے" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rاﷲ آپ کو ایسی ہزار کامیابیاں عطا فرمائے";
    if ("جانتا ہوں آپ کا دل بہت بڑا ہے اس لیے آپ میری خطا کو نظر انداز کر کے مجھے معاف کر دیں گے" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rجانتا ہوں آپ کا دل بہت بڑا ہے اس لیے آپ میری خطا کو نظر انداز کر کے مجھے معاف کر دیں گے";
    if ("آپ بہت اچھے ہیں آپ سے معافی کی امید رکھتا ہوں" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rآپ بہت اچھے ہیں آپ سے معافی کی امید رکھتا ہوں";
    if ("میں اپنی خطا مانتے ہوئے آپ سے معافی کا طلبگار ہوں" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rمیں اپنی خطا مانتے ہوئے آپ سے معافی کا طلبگار ہوں";
    if ("میں دعا گو ہوں کہ آپ ہمیشہ مسکراہٹوں کے موتی بکھیرتے رہیں" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rمیں دعا گو ہوں کہ آپ ہمیشہ مسکراہٹوں کے موتی بکھیرتے رہیں";
    if ("زندگی ہمیشہ آپ کی مسکراتے ہوئے گزرے" == a) document.getElementById("txt1").value = document.getElementById("txt1").value + "\rزندگی ہمیشہ آپ کی مسکراتے ہوئے گزرے";
    if ("ہم سب کی طرف سے آپکو عید مبارک" == a) document.getElementById("txt1").value = "ہم سب کی طرف سے آپکو عید مبارک";
    if ("ہم سب کی طرف سے آپکو عید مبارک" == a) document.getElementById("txt1").value = "ہم سب کی طرف سے آپکو عید مبارک";
    if ("ہم سب کی طرف سے آپکو عید مبارک" == a) document.getElementById("txt1").value = "ہم سب کی طرف سے آپکو عید مبارک";
    if ("ہم سب کی طرف سے آپکو عید مبارک" == a) document.getElementById("txt1").value = "ہم سب کی طرف سے آپکو عید مبارک";
    if ("دل کی گہرائیوں سے عید مبارک" == a) document.getElementById("txt1").value = "دل کی گہرائیوں سے عید مبارک";
    if ("ہم سب کی طرف سے دلی عید مبارک" == a) document.getElementById("txt1").value = "ہم سب کی طرف سے دلی عید مبارک";
    if ("نیک تمناؤں کے ساتھ عید مبارک" == a) document.getElementById("txt1").value = "نیک تمناؤں کے ساتھ عید مبارک";
    if ("آپ کو خوشیوں بھری عید مبارک ہو" == a) document.getElementById("txt1").value = "آپ کو خوشیوں بھری عید مبارک ہو";
}

function hurf2(a) {
    if ("alifmada" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "آ";
    if ("alif" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ا";
    if ("bay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ب";
    if ("pay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "پ";
    if ("tay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ت";
    if ("thay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ٹ";
    if ("say" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ث";
    if ("jim" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ج";
    if ("hay1" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ح";
    if ("khay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "خ";
    if ("chay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "چ";
    if ("dal" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "د";
    if ("dhal" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ڈ";
    if ("zal" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ذ";
    if ("ray" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ر";
    if ("zay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ز";
    if ("kaf" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ک";
    if ("gaf" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "گ";
    if ("qaf" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ق";
    if ("fay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ف";
    if ("gain" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "غ";
    if ("ain" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ع";
    if ("zhoy" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ظ";
    if ("thoy" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ط";
    if ("duad" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ض";
    if ("suad" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ص";
    if ("sheen" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ش";
    if ("seen" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "س";
    if ("zhay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ژ";
    if ("rahy" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ڑ";
    if ("pbuh" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ﷺ";
    if ("Allah" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "لله";
    if ("bariya" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ے";
    if ("yamada" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ئ";
    if ("ya" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ی";
    if ("dochachmihay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ھ";
    if ("hay" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ہ";
    if ("hamza" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ء";
    if ("wowmada" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ؤ";
    if ("wow" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "و";
    if ("gunah" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ں";
    if ("noon" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ن";
    if ("mim" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "م";
    if ("lam" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "ل";
    if ("space" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + " ";
    if ("entr" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\r";
    if ("bspace" == a) {
        varbk = document.getElementById("txt2").value;
        varbk = varbk.substring(0, varbk.length - 1);
        document.getElementById("txt2").value = varbk;
    }
    if ("dash" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "۔";
    if ("salam" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rاسلام علیکم";
    if ("دل کی گہرائیوں سے دلی عید مبارک" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rدل کی گہرائیوں سے دلی عید مبارک۔";
    if ("ہم سب کی طرف سے دلی عید مبارک" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rہم سب کی طرف سے دلی عید مبارک۔";
    if ("آپ کو نیک دعاؤں اور تمناؤں کے ساتھ عید مبارک" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rآپ کو نیک دعاؤں اور تمناؤں کے ساتھ عید مبارک۔";
    if ("آپ کو خوشیوں سے بھرپور عید مبارک ہو" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rآپ کو خوشیوں سے بھرپور عید مبارک ہو۔";
    if ("رحمتوں اور برکتوں والا مہینہ مبارک ہو" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rرحمتوں اور برکتوں والا مہینہ مبارک ہو۔";
    if ("مغفرتوں والا مہینہ مبارک ہو" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rمغفرتوں والا مہینہ مبارک ہو۔";
    if ("اﷲ آپ کو اس مہینے میں ہزاروں خوشیاں عطا فرمائے" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rاﷲ آپ کو اس مہینے میں ہزاروں خوشیاں عطا فرمائے۔";
    if ("اﷲ آپ کو اس مہینے میں ہزاروں برکتیں و نعمتیں عطا فرمائے" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rاﷲ آپ کو اس مہینے میں ہزاروں برکتیں و نعمتیں عطا فرمائے۔";
    if ("سالگرہ مبارک" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rسالگرہ مبارک";
    if ("آپ کو سالگرہ کا دن مبارک ہو" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rآپ کو سالگرہ کا دن مبارک ہو";
    if ("میری دعا ہے کہ آپ کو ہزاروں سالگرہ نصیب ہوں" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rمیری دعا ہے کہ آپ کو ہزاروں سالگرہ نصیب ہوں";
    if ("مجھے امید ہے آپ کی زندگی ہمیشہ خوشیوں سے بھری ہوگی" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rمجھے امید ہے آپ کی زندگی ہمیشہ خوشیوں سے بھری ہوگی";
    if ("کاش آپ ابھی میری زندگی میں آجاؤ" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rکاش آپ ابھی میری زندگی میں آجاؤ";
    if ("آپ کی یاد دل کو تڑپا رہی ہے" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rآپ کی یاد دل کو تڑپا رہی ہے";
    if ("آدھے آدھے تھے ہم دونوں مل جاتے تو پورے ہوتے" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rآدھے آدھے تھے ہم دونوں\rمل جاتے تو پورے ہوتے";
    if ("آپ کی نظرِ کرم کا شکریہ" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rآپ کی نظرِ کرم کا شکریہ";
    if ("ہمیں یاد کرنے پر دل و جان سے آپ کا شکریہ" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rہمیں یاد کرنے پر دل و جان سے آپ کا شکریہ";
    if ("میں بہت خوش قسمت ہوں کہ مجھے آپ جیسا بہترین دوست ملا" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rمیں بہت خوش قسمت ہوں کہ مجھے آپ جیسا بہترین دوست ملا";
    if ("میں اﷲ سے دعا کرتا ہوں کہ میری اور آپ کی دوستی ہمیشہ سلامت رہے" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rمیں اﷲ سے دعا کرتا ہوں کہ میری اور آپ کی دوستی ہمیشہ سلامت رہے";
    if ("خدا کرے میری اور آپ کی دوستی کو کسی کی نظر نہ لگے" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rخدا کرے میری اور آپ کی دوستی کو کسی کی نظر نہ لگے";
    if ("آپ کو یہ کامیابی مبارک ہو" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rآپ کو یہ کامیابی مبارک ہو";
    if ("اﷲ آپ کو ایسی ہزار کامیابیاں عطا فرمائے" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rاﷲ آپ کو ایسی ہزار کامیابیاں عطا فرمائے";
    if ("جانتا ہوں آپ کا دل بہت بڑا ہے اس لیے آپ میری خطا کو نظر انداز کر کے مجھے معاف کر دیں گے" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rجانتا ہوں آپ کا دل بہت بڑا ہے اس لیے آپ میری خطا کو نظر انداز کر کے مجھے معاف کر دیں گے";
    if ("آپ بہت اچھے ہیں آپ سے معافی کی امید رکھتا ہوں" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rآپ بہت اچھے ہیں آپ سے معافی کی امید رکھتا ہوں";
    if ("میں اپنی خطا مانتے ہوئے آپ سے معافی کا طلبگار ہوں" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rمیں اپنی خطا مانتے ہوئے آپ سے معافی کا طلبگار ہوں";
    if ("میں دعا گو ہوں کہ آپ ہمیشہ مسکراہٹوں کے موتی بکھیرتے رہیں" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rمیں دعا گو ہوں کہ آپ ہمیشہ مسکراہٹوں کے موتی بکھیرتے رہیں";
    if ("زندگی ہمیشہ آپ کی مسکراتے ہوئے گزرے" == a) document.getElementById("txt2").value = document.getElementById("txt2").value + "\rزندگی ہمیشہ آپ کی مسکراتے ہوئے گزرے";
    if ("ہم سب کی طرف سے آپکو عید مبارک" == a) document.getElementById("txt2").value = "ہم سب کی طرف سے آپکو عید مبارک";
    if ("ہم سب کی طرف سے آپکو عید مبارک" == a) document.getElementById("txt2").value = "ہم سب کی طرف سے آپکو عید مبارک";
    if ("ہم سب کی طرف سے آپکو عید مبارک" == a) document.getElementById("txt2").value = "ہم سب کی طرف سے آپکو عید مبارک";
    if ("ہم سب کی طرف سے آپکو عید مبارک" == a) document.getElementById("txt2").value = "ہم سب کی طرف سے آپکو عید مبارک";
    if ("دل کی گہرائیوں سے عید مبارک" == a) document.getElementById("txt2").value = "دل کی گہرائیوں سے عید مبارک";
    if ("ہم سب کی طرف سے دلی عید مبارک" == a) document.getElementById("txt2").value = "ہم سب کی طرف سے دلی عید مبارک";
    if ("نیک تمناؤں کے ساتھ عید مبارک" == a) document.getElementById("txt2").value = "نیک تمناؤں کے ساتھ عید مبارک";
    if ("آپ کو خوشیوں بھری عید مبارک ہو" == a) document.getElementById("txt2").value = "آپ کو خوشیوں بھری عید مبارک ہو";
}