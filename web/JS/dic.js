var currEdit = null;
var componentId='';
var charSpace = String.fromCharCode(32);

var charEnter = String.fromCharCode(13);

var charTab = String.fromCharCode(9);

var charNewLine = "\n";

var charColon = String.fromCharCode(58);

var charSemiColon = String.fromCharCode(59);

var charSingleQuote = String.fromCharCode(39);

var charDoubleQuote = String.fromCharCode(34);

var charFullStop = String.fromCharCode(46);

var charComma = String.fromCharCode(44);

var charExclaim = String.fromCharCode(33);

var charPlus = String.fromCharCode(43);

var charMinus = String.fromCharCode(45);

var charMul = String.fromCharCode(42);

var charDiv = String.fromCharCode(47);

var charPrecent = String.fromCharCode(37);

var charLeftParen = String.fromCharCode(41);

var charRightParen = String.fromCharCode(40);

var charEqual = String.fromCharCode(61);

var charDecSep = String.fromCharCode(61);

var codes = new Array();

codes["a"] = 1575;

codes["b"] = 1576;

codes["c"] = 1670;

codes["d"] = 1583;

codes["e"] = 1593;

codes["f"] = 1601;

codes["g"] = 1711;

codes["h"] = 1726;

codes["i"] = 1740;

codes["j"] = 1580;

codes["k"] = 1705;

codes["l"] = 1604;

codes["m"] = 1605;

codes["n"] = 1606;

codes["o"] = 1729;

codes["p"] = 1662;

codes["q"] = 1602;

codes["r"] = 1585;

codes["s"] = 1587;

codes["t"] = 1578;

codes["u"] = 1574;

codes["v"] = 1591;

codes["w"] = 1608;

codes["x"] = 1588;

codes["y"] = 1746;

codes["z"] = 1586;

codes["A"] = 1570;

codes["B"] = 1576;

codes["C"] = 1579;

codes["D"] = 1672;

codes["E"] = 1617;

codes["F"] = 1613;

codes["G"] = 1594;

codes["H"] = 1581;

codes["I"] = 1648;

codes["J"] = 1590;

codes["K"] = 1582;

codes["L"] = 1576;

codes["M"] = 1611;

codes["N"] = 1722;

codes["O"] = 1576;

codes["P"] = 1615;

codes["Q"] = 1576;

codes["R"] = 1681;

codes["S"] = 1589;

codes["T"] = 1657;

codes["U"] = 1569;

codes["V"] = 1592;

codes["W"] = 1572;

codes["X"] = 1688;

codes["Z"] = 1584;

codes[">"] = 1616;

codes["<"] = 1614;

codes[charSpace] = 32;

codes[charEnter] = 13;

codes[charColon] = 1563;

codes[charSemiColon] = 1563;

codes[charSingleQuote] = 8216;

codes[charDoubleQuote] = 8220;

codes[charFullStop] = 1748;

codes[charComma] = 1548;

codes[charExclaim] = 33;

codes["?"] = 1567;

codes[":"] = 58;

codes[charPlus] = 43;

codes[charMinus] = 45;

codes[charMul] = 215;

codes[charDiv] = 247;

codes[charPrecent] = 1642;

codes[charLeftParen] = 40;

codes[charRightParen] = 41;

codes[charEqual] = 61;

codes["0"] = 1632;

codes["1"] = 1633;

codes["2"] = 1634;

codes["3"] = 1635;

codes["4"] = 1636;

codes["5"] = 1637;

codes["6"] = 1638;

codes["7"] = 1639;

codes["8"] = 1640;

codes["9"] = 1641;

function register() {
    if (!document.all) {
        var a = Form1.txt1;
        if (navigator.userAgent.toLowerCase().indexOf("chrome") == -1) addEvent(a, "keypress", search);
    }
}

function addEvent(a, b, c) {
    if (a.addEventListener) {
        a.addEventListener(b, c, true);
        return true;
    } else if (a.attachEvent) {
        alert("on" + b);
        var d = a.attachEvent("on" + b, c);
        return d;
    } else alert("Handler could not be attached");
}

function search(a,componentId) {
    a = a ? a : window.event ? event : null;
    if (a) {
        var b = a.charCode ? a.charCode : a.keyCode ? a.keyCode : a.which ? a.which : 0;
        var c = b;
        var d = String.fromCharCode(c);
        if (a.keyCode) {
            a.keyCode = codes[d];
            if (navigator.userAgent.toLowerCase().indexOf("chrome") > -1) {
                AddText(String.fromCharCode(codes[d]),componentId);
                a.preventDefault();
                a.stopPropagation();
            }
        } else if (a.which) {
            AddText(String.fromCharCode(codes[d]),componentId);
            a.preventDefault();
            a.stopPropagation();
        } else if (a.charCode) {
            AddText(d,componentId);
            a.preventDefault();
            a.stopPropagation();
        }
    }
}

function AddText(a,componentId) {
    var b = document.getElementById(componentId).value;
    b += a;
    document.getElementById(componentId).value = b;
    document.getElementById(componentId).scrollTop = document.getElementById(componentId).scrollHeight;
}

function inputtextsearch(a,ab) {
    a = a ? a : window.event ? event : null;
    if (a) {
        var b = a.charCode ? a.charCode : a.keyCode ? a.keyCode : a.which ? a.which : 0;
        var c = b;
        var d = String.fromCharCode(c);
        if (a.keyCode) {
            a.keyCode = codes[d];
            if (navigator.userAgent.toLowerCase().indexOf("chrome") > -1) {
                AddTextinputtext(String.fromCharCode(codes[d]));
                a.preventDefault();
                a.stopPropagation();
            }
        } else if (a.which) {
            AddTextinputtext(String.fromCharCode(codes[d]));
            a.preventDefault();
            a.stopPropagation();
        } else if (a.charCode) {
            AddTextinputtext(d);
            a.preventDefault();
            a.stopPropagation();
        }
    }
}

function AddTextinputtext(a) {
    var b = document.getElementById("txt2").value;
    b += a;
    document.getElementById("txt2").value = b;
    document.getElementById("txt2").scrollTop = document.getElementById("txt1").scrollHeight;
}